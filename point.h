#ifndef _POINT_H
#define _POINT_H
#include <stdbool.h>

struct point{
  float x;
  float y;
  float z;
};
bool pointEquals(struct point* p1, struct point* p2);
int pointNumberOf(struct point* array[], int size);
#endif

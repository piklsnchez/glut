#ifndef __DRAW_UTIL_H
#define __DRAW_UTIL_H

struct DrawUtil {
  void (*drawAt)(void* shape, char* type, Rectangle_t* position);
} typedef DrawUtil_t;

DrawUtil_t* DrawUtil_init();

#endif

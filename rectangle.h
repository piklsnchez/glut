#ifndef _RECTANGLE_H
#define _RECTANGLE_H
struct rectangle {
  struct dimension dimension;
  struct point     point;
};
bool rectangleContains(struct rectangle* haystack, struct point* needle);
#endif

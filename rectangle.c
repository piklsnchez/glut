#include "point.h"
#include "dimension.h"
#include "rectangle.h"

bool rectangleContains(struct rectangle* haystack, struct point* needle){
  if(   needle->x >= haystack->point.x
     && needle->x <= haystack->point.x + haystack->dimension.width
     && needle->y <= haystack->point.y
     && needle->y >= haystack->point.y - haystack->dimension.height
  ){
    return true;
  }
  return false;
}

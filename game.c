#include <stdlib.h>//malloc
#include <stdio.h>//printf
#include <stdbool.h>
#include <math.h>//fmodf
#include <time.h>
#include <GL/glut.h>//gl
#include <GL/freeglut.h>//glutBitmapString
#include <gc.h>

#include "point.h"//point
#include "dimension.h"
#include "rectangle.h"

#define VISUAL_DEBUG false

#define PLAYER_MISSILES_SIZE 10
#define ENEMIES_SIZE 15
#define ENEMIES_MISSILES_SIZE 5

struct thing {
  int               timeout;
  struct dimension* screen;
  struct point      player;
  float             player_speed;
  struct point*     player_missiles[PLAYER_MISSILES_SIZE];
  float             player_missilesSpeed;
  struct point*     enemies[ENEMIES_SIZE];
  float             enemies_speed;
  float             enemies_offset;
  int               enemies_direction;
  int               enemies_perRow;
  float             enemies_verticalSpacing;
  struct point*     enemies_missiles[ENEMIES_MISSILES_SIZE];
  float             enemies_missilesSpeed;
};

struct thing t;

struct dimension* getScreenSize();
void changeSize(int w, int h);
void renderScene();
void keyboard(unsigned char key, int x, int y);
void altKeyboard(int key, int x, int y);
void action(int timeout);
void initPlayer();
void initEnemies();
void moveMissiles();
void moveEnemies();
bool detectCollision();
struct rectangle* getHitBox(struct point* p);

int main(int argc, char* argv[]) {
  glutInit(&argc, argv);

  t.screen  = getScreenSize();
  t.timeout = 30;
  srand((unsigned)time(NULL));
  
  initPlayer();
  initEnemies();
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(t.screen->width, t.screen->height);
  glutCreateWindow("glut test");
  glutDisplayFunc(renderScene);
  glutReshapeFunc(changeSize);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(altKeyboard);
  glutTimerFunc(0, action, 0);
  glutMainLoop();

  return 1;
}

void initPlayer(){
  t.player_speed         = 0.03;
  t.player_missilesSpeed = 0.03;
  t.player.x             = 0.0;
  t.player.y             = -0.7;

  for(int i = 0; i < PLAYER_MISSILES_SIZE; i++){
    t.player_missiles[i] = NULL;
  }
}

void initEnemies(){
  t.enemies_speed           = 0.01;
  t.enemies_missilesSpeed   = 0.03;
  t.enemies_offset          = 0.0;
  t.enemies_direction       = 1;//move to the right
  t.enemies_perRow          = 5;
  t.enemies_verticalSpacing = 4.0;

  float leftOffset = -1.0;
  float width      = 2.0;//center them with some room on the side
  for(int i = 0; i < ENEMIES_SIZE; i++){
    t.enemies[i] = GC_malloc(sizeof(struct point));
    float pos = (i / (t.enemies_perRow / width));
    t.enemies[i]->x = fmodf(pos, width) + leftOffset;
    int y = pos / width;
    t.enemies[i]->y = 1 - (y / t.enemies_verticalSpacing);
  }

  for(int i = 0; i < ENEMIES_MISSILES_SIZE; i++){
    t.enemies_missiles[i] = NULL;
  }
}

void action(int value) {
  moveMissiles();
  moveEnemies();
  detectCollision();
  glutPostRedisplay();
  glutTimerFunc(t.timeout, action, 0);
}

struct dimension* getScreenSize(){
  struct dimension* size = GC_malloc(sizeof(struct dimension));
  size->width            = glutGet(GLUT_SCREEN_WIDTH) / 2;//multimonitor
  size->height           = glutGet(GLUT_SCREEN_HEIGHT) / 2;//multimonitor
  return size;
}

void changeSize(int w, int h) {
  printf("width: %i, height: %i\n", w, h);
  if (h == 0){
    h = 1;
  }
  float ratio =  w * 1.0 / h;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, w, h);
  gluOrtho2D(-1.0 * ratio, 1.0 * ratio, -1.0, 1.0);
}

void renderScene(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBegin(GL_TRIANGLES);//player
    glVertex2f(0.00 + t.player.x, 0.00 + t.player.y);
    glVertex2f(0.03 + t.player.x, -0.03 + t.player.y);
    glVertex2f(-0.03 + t.player.x, -0.03 + t.player.y);
  glEnd();

  for(int i = 0; i < PLAYER_MISSILES_SIZE; i++){
    if(NULL != t.player_missiles[i]){
      glBegin(GL_TRIANGLES);//missile
        glVertex2f(0.00 + t.player_missiles[i]->x, 0.00 + t.player_missiles[i]->y);
        glVertex2f(0.01 + t.player_missiles[i]->x, -0.01 + t.player_missiles[i]->y);
        glVertex2f(-0.01 + t.player_missiles[i]->x, -0.01 + t.player_missiles[i]->y);
      glEnd();
//debug
//      char message[55];
//      snprintf(message, 54, "missile x:%2.1f, y:%2.1f", t.missiles[i]->x, t.missiles[i]->y);
//      glRasterPos2f(0.0, 0.0);
//      glutBitmapString(GLUT_BITMAP_9_BY_15, message);
//end debug
    }
  }

  for(int i = 0; i < ENEMIES_SIZE; i++){
    if(NULL != t.enemies[i]){
      //draw hit box
      if(VISUAL_DEBUG){
        float currentColor[4];
        glGetFloatv(GL_CURRENT_COLOR,currentColor);
        glColor3f(1.0, 0.0, 0.0);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        struct rectangle* hitBox = getHitBox(t.enemies[i]);
        glRectf(hitBox->point.x + t.enemies_offset, hitBox->point.y, hitBox->point.x + hitBox->dimension.width + t.enemies_offset, hitBox->point.y - hitBox->dimension.height);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3]);
      }
      glBegin(GL_TRIANGLES);//enemies
        glVertex2f((0.00 + t.enemies[i]->x) + t.enemies_offset, 0.00 + t.enemies[i]->y);
        glVertex2f((0.03 + t.enemies[i]->x) + t.enemies_offset, -0.03 + t.enemies[i]->y);
        glVertex2f((-0.03 + t.enemies[i]->x) + t.enemies_offset, -0.03 + t.enemies[i]->y);
      glEnd();
    }
  }

  for(int i = 0; i < ENEMIES_MISSILES_SIZE; i++){
    if(NULL != t.enemies_missiles[i]){
      glBegin(GL_TRIANGLES);//enemiesMissiles
        glVertex2f(0.0 + t.enemies_missiles[i]->x, 0.0 + t.enemies_missiles[i]->y);
        glVertex2f(0.01 + t.enemies_missiles[i]->x, -0.01 + t.enemies_missiles[i]->y);
        glVertex2f(-0.01 + t.enemies_missiles[i]->x, -0.01 + t.enemies_missiles[i]->y);
      glEnd();
    }
  }
  glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y){
  switch(key){
    case ' ':
      for(int i = 0; i < PLAYER_MISSILES_SIZE; i++){
        if(NULL == t.player_missiles[i]){
          t.player_missiles[i] = GC_malloc(sizeof(struct point));
          t.player_missiles[i]->x = t.player.x;
          t.player_missiles[i]->y = t.player.y;
          break;
        }
      }
    break;
    deafult:
      printf("char: %c; x: %i; y: %i;\n", key, x, y);
  }
}

void altKeyboard(int key, int x, int y){
  switch(key){
    case GLUT_KEY_LEFT:
      t.player.x -= t.player_speed;
    break;
    case GLUT_KEY_RIGHT:
      t.player.x += t.player_speed;
    break;
    case GLUT_KEY_UP:
      t.player.y += t.player_speed;
    break;
    case GLUT_KEY_DOWN:
      t.player.y -= t.player_speed;
    break;
    default:
      printf("key: %i\n", key);
  }
}

void moveMissiles(){
  //player missile
  for(int i = 0; i < PLAYER_MISSILES_SIZE; i++){
    if(NULL == t.player_missiles[i]){
      continue;
    }
    if(t.player_missiles[i]->y < 1.0){
      t.player_missiles[i]->y += t.player_missilesSpeed;
    } else {
      GC_free(t.player_missiles[i]);
      t.player_missiles[i] = NULL;
    }
  }
  
  for(int i = 0; i < ENEMIES_MISSILES_SIZE; i++){
    int r = rand() % ENEMIES_SIZE;//pick a random enemy
    if(NULL != t.enemies[r] && NULL == t.enemies_missiles[i]){
      t.enemies_missiles[i]    = GC_malloc(sizeof(struct point));//create the missile
      t.enemies_missiles[i]->x = t.enemies[r]->x + t.enemies_offset;
      t.enemies_missiles[i]->y = t.enemies[r]->y;
    }
    if(NULL != t.enemies_missiles[i]){
      if(t.enemies_missiles[i]->y > -1.0){
        t.enemies_missiles[i]->y -= t.enemies_missilesSpeed;
      } else {
        GC_free(t.enemies_missiles[i]);
        t.enemies_missiles[i] = NULL;
      }
    }
  }
}

void moveEnemies(){
  //TODO: calculate boundries
  if(t.enemies_offset <= -0.75 || t.enemies_offset >= 0.75){
    t.enemies_direction *= -1;
  }
  t.enemies_offset += t.enemies_speed * t.enemies_direction;
}

bool detectCollision(){
  bool result = false;
  //player missile
  for(int i = 0; i < ENEMIES_SIZE; i++){
    for(int j = 0; j < PLAYER_MISSILES_SIZE; j++){
      if(NULL == t.enemies[i] || NULL == t.player_missiles[j]){
        continue;
      }
      struct point p;
      p.x = t.enemies[i]->x + t.enemies_offset;
      p.y = t.enemies[i]->y;
      struct rectangle* hitBox = getHitBox(&p);
      if(/*rectangle*/rectangleContains(hitBox, t.player_missiles[j])){
        GC_free(t.enemies[i]);
        t.enemies[i] = NULL;
        if(0 == /*point*/pointNumberOf(t.enemies, ENEMIES_SIZE)){
          //doWin(screenSize);
          printf("you win\n");
          exit(0);
        }
        GC_free(t.player_missiles[j]);
        t.player_missiles[j] = NULL;
        result = true;
      }
    }
  }
  //enemies missile
//TODO: finish this
  for(int i = 0; i < ENEMIES_MISSILES_SIZE; i++){
    if(NULL == t.enemies_missiles[i]){
      continue;
    }
    struct rectangle* hitBox = getHitBox(&t.player);
    if(/*rectangle*/rectangleContains(hitBox, t.enemies_missiles[i])){
      GC_free(t.enemies_missiles[i]);
      t.enemies_missiles[i] = NULL;
      //doLose(screenSize);
      printf("you lose\n");
      exit(0);
    }
  }
  return result;
}

/*private*/
struct rectangle* getHitBox(struct point* p){
  float padding            = 0.03;//TODO: get rid of the magic
  struct rectangle* hitBox = GC_malloc(sizeof(struct rectangle));
  hitBox->point.x          = p->x - padding;
  hitBox->point.y          = p->y + padding;
  hitBox->dimension.width  = padding * 2;
  hitBox->dimension.height = padding * 2;
  return hitBox;
}
